Studentnummer:327764
Naam: Justin Bakker	

Hoe open je de Terminal in Visual Studio Code?
	# CTRL+SHIFT+` of boven in klikken op terminal

Met welk commando kan je checken of welke bestanden zijn toegevoegd aan de commit en welke niet?
	# git status

Wat is het commando van een multiline commit message?
	# git commit -m "line1" -m "line2"

Hoeveel commando's heb je in opdracht 4a uitgevoerd?
	# 4

Zoek het volgende commando op:
 - 1 commit teruggaan in de commit history. (reset)
	# git log , git checkout <COMMITID> , git reset --hard <COMMITID

	of

	git restore --staged <file>
